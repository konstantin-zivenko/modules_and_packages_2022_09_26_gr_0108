# from <module name> import <name> [, <name _N>]
# from <module name> import <name> as <alt name>

from mod import s, foo
from math import pi as PI
# import <module name> as <new module name>

foo(s)
pi = 10
print(s)

foo(s)

print(pi, PI)

def bar():
    from mod import foo
    # from mod import * - not working
    foo('ggggg')



